class ARBOL4
{
    constructor()
    {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 3;
        this.buscarNodos = [];
    }

    agregarNodoPadre(){
        var nodo = new NODO4(null,null,"+",100);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre,valor){
        var nodo = new NODO4(nodoPadre,posicion,nombre,valor);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }
    buscarvalor(BuscarElemento,nodo  = this.nodoPadre){

        if(nodo.valor == BuscarElemento)
            return  nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarvalor(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarvalor(nodo.hD);


    }
}