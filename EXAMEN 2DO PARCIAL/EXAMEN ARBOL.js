class EXAMENARBOL
{
    constructor() {
        this.Padre=this.NODOPADRE;

        //this.nivel=5;
        //this.busquedanivel=[];
        //this.buselemento;
        //this.buscamino='';
        //this.sumvalores=0;
    }

    AGREGARNODO(nombre,posicion,nivel,hizq,hder,valor,padre)
    {
        var NODO = new EXAMENNODO(nombre,posicion,nivel,hizq,hder,valor,padre)
        return NODO;
    }

    NODOPADRE(nombre,posicion,nivel,hizq,hder,valor,padre)
    {
        var NOD = new EXAMENNODO(nombre,posicion,nivel,hizq,hder,valor,padre)
        return NOD;
    }

    busnivel(nodo)
    {
        if(nodo.nivel==this.nivel)
            this.busquedanivel.push(nodo.valor);

        if(nodo.hasOwnProperty('hizq'))
            this.busnivel(nodo.hizq);

        if(nodo.hasOwnProperty('hder'))
            this.busnivel(nodo.hder);

        return this.busquedanivel;
    }
    busvalor(buselemento, nodo)
    {
        if(nodo.nombre==buselemento)
            this.buselemento=nodo;

        if(nodo.hasOwnProperty('hizq'))
            this.busvalor(buselemento,nodo.hizq);

        if(nodo.hasOwnProperty('hder'))
            this.busvalor(buselemento,nodo.hder);

        return this.buselemento;
    }

    buscaminodo(nodo)
    {
        if(nodo.padre!=null)
        {

            this.buscamino=this.buscamino + ' ' +nodo.padre.nombre;

            this.buscaminodo(nodo.padre)
        }
        return this.buselemento.nombre + ' ' + this.buscamino;
    }
    sumavar(nodo)
    {
        if(nodo.padre!=null)
        {

            this.sumvalores=this.sumvalores + nodo.padre.valor;
            this.sumavar(nodo.padre)
        }
        return this.buselemento.valor + this.sumvalores;

    }



}