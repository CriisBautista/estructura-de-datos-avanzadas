class ARBOL10
{
    constructor()
    {
        this.nodpadre=this.nodopadre;
        //this.nivel=3;
        //this.buscarnivel=[];
        this.elemento;
        this.camino='';
        this.sumavalores=0;
    }

    agregarnodo(nombre,valor,nivel,hijoi,hijod,val,padre)
    {
        var nod=new NODO10(nombre,valor,nivel,hijoi,hijod,val,padre)
        return nod
    }

    nodopadre(nombre,valor,nivel,hijoi,hijod,val,padre)
    {
        var nodo=new NODO10(nombre,valor,nivel,hijoi,hijod,val,padre);
        return nodo;
    }

    niveles(nodo)
    {
        if(nodo.nivel==this.nivel)
            this.buscarnivel.push(nodo.val);

        if(nodo.hasOwnProperty('hijoi'))
            this.niveles(nodo.hijoi);

        if(nodo.hasOwnProperty('hijod'))
            this.niveles(nodo.hijod);

        return this.buscarnivel;
    }

    busquedavalor(elemento, nodo)
    {
        if(nodo.val==elemento)
            this.elemento=nodo;

        if(nodo.hasOwnProperty('hijoi'))
            this.busquedavalor(elemento,nodo.hijoi);

        if(nodo.hasOwnProperty('hijod'))
            this.busquedavalor(elemento,nodo.hijod);

        return this.elemento;
    }

    caminodo(nodo)
    {
        if(nodo.padre!=null)
        {

            this.camino=this.camino + ' ' +nodo.padre.nombre;

            this.caminodo(nodo.padre)
        }
        return this.elemento.nombre + ' ' + this.camino;
    }
    suma(nodo)
    {
        if(nodo.padre!=null)
        {

            this.sumavalores=this.sumavalores + nodo.padre.val;
            // console.log(this.camino)
            this.suma(nodo.padre)
        }
        return this.elemento.val + this.sumavalores;

    }



}