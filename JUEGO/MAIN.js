let turno=0;
const mesa=[];

const btnpulsado =(e,lugar) =>
{
    turno++;
    const btn=e.target;
    const jugador=turno % 2 ? 'JUGADOR 1 ': 'JUGADOR 2';
    const letras=turno % 2 ? 'X':'O';
    btn.textContent=letras;
    const color=turno %2 ? 'yellow':'pink';
    btn.style.background=color;

    mesa[lugar]=color;

    if(ganador()) alert('HA GANADO EL ' + jugador);

}

const ganador=()=>
{
    if(mesa[0]==mesa[1] && mesa[0]==mesa[2] && mesa[0])
    {
        return true
    }
    else if(mesa[3]==mesa[4] && mesa[3]==mesa[5] && mesa[3])
    {
        return true
    }
    else if(mesa[6]==mesa[7] && mesa[6]==mesa[8] && mesa[6])
    {
        return true
    }
    else if(mesa[0]==mesa[3] && mesa[0]==mesa[6] && mesa[0])
    {
        return true
    }
    else if(mesa[1]==mesa[4] && mesa[1]==mesa[7] && mesa[1])
    {
        return true
    }
    else if(mesa[2]==mesa[5] && mesa[2]==mesa[8] && mesa[2])
    {
        return true
    }
    else if(mesa[0]==mesa[4] && mesa[0]==mesa[8] && mesa[0])
    {
        return true
    }
    else if(mesa[2]==mesa[4] && mesa[2]==mesa[6] && mesa[2])
    {
        return true
    }
}




document.querySelectorAll('button').forEach((obj,i) => obj.addEventListener('click', (e) => btnpulsado(e,i)))