const monto = document.getElementById('monto');
const tiempo = document.getElementById('tiempo');
const interes = document.getElementById('interes');
const btnCalcular = document.getElementById('btnCalcular');

const llenartabla= document.querySelector('#lista-tabla tbody')

btnCalcular.addEventListener('click', () => {
    calcularcuota(monto.value, interes.value, tiempo.value);
})

function calcularcuota(monto, interes, tiempo)
{


    let fecha=[];
    let fechasig=[];
    let fechaact=Date.now();
    let mesact=moment(fechaact);
    let messig=moment(fechaact);
    let dias=0;

    //mesact.add(1,'month');
    messig.add(1, 'month');


    let pagointeres=0, capital=0, cuota=0,periodo=0,iva=0;



    cuota=monto/tiempo;

    for (let i=1; i<=tiempo;i++)
    {

        periodo++;

        //fecha
        fecha[i]=mesact.format('DD-MM-YYYY');
        fechasig[i]=messig.format('DD-MM-YYYY');
        dias=(messig.diff(mesact, 'days'));
        mesact.add(1, 'month');
        messig.add(1, 'month');
        pagointeres=parseFloat(((monto*interes)/360)*dias);
        iva=pagointeres*.16;
        capital=cuota+pagointeres+iva;
        monto=parseFloat(monto-cuota);

        const row=document.createElement('tr')
        row.innerHTML=`
            <td>${periodo}</td>
            <td>${fecha[i]}</td>
            <td>${fechasig[i]}</td>
            <td>${dias}</td>
            <td></td>
            <td>${monto.toFixed(2)}</td>
            <td>${cuota.toFixed(2)}</td>
            <td>${pagointeres.toFixed(2)}</td>
            <td>${iva.toFixed(2)}</td>
            <td>${capital.toFixed(2)}</td>
        `;

        llenartabla.appendChild(row);
    }
}

