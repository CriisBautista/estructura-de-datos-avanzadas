class NODO
{
    constructor(tipo,valor,nivel,padre,hijoi=0,hijod=0)
    {
        this.tipo=tipo;
        this.valor=valor;
        this.nivel=nivel;
        this.hijoi=hijoi;
        this.hijod=hijod;

        if(hijoi==1)
            this.hijoizq=this.createhijo("i",0,nivel+1, this.nivel);
        if (hijod==1)
            this.hijoder=this.createhijo("d",0,nivel+1, this.nivel);
    }

    createhijo(tipo,valor,nivel,padre)
    {
        var createhijo=new NODO(tipo,valor,nivel,padre);
        return createhijo;
    }
}